import sys, os, glob, fnmatch, shutil,commands,re
src_dir = "/Volumes/HD/canlab/7T_ketamine/ketamin"
subjids = os.listdir(src_dir)
subjids.remove('abnormal')
subjids.sort()
#subjids = ['bg52']
for subjid in subjids:
	subjpath = os.path.join(src_dir,subjid)
	if not subjid.startswith('.') and os.path.isdir(subjpath):
		messungs = ['1_Messung','2_Messung','3_Messung']
		for messung in messungs:
			print subjid + ':' + messung
			subj_src_dir_messung = os.path.join(subjpath,messung)
			os.chdir(subj_src_dir_messung)

			for dcmtarpath, dcmtardirs, dcmtarfiles in os.walk(subj_src_dir_messung):
				matching_tar = [s for s in dcmtarfiles if ".tar" in s]
				matching_uncomoress = [um for um in dcmtardirs 
										if "dicom" in um and os.path.isdir(os.path.join(dcmtarpath, um))]
				if len(matching_tar) == 1 and len (matching_uncomoress) == 0:
						print dcmtarpath
						# dcmtar = os.path.join(dcmtarpath,matching[0])
						os.chdir(dcmtarpath)
						commands.getstatusoutput('tar -zxvf *.tar')


			for dcmtarpath, dcmtardirs, dcmtarfiles in os.walk(subj_src_dir_messung):
				if "study" in dcmtarpath:
					print dcmtarpath
						
					sorteddcm = os.path.join(os.path.dirname(os.path.dirname(dcmtarpath)),'dcm_sorted')
					if not os.path.exists(sorteddcm):
						os.mkdir(sorteddcm)
						dcmsorter = ("/Users/canlab06/Documents/Soft/dcmsort.sh -D %s -o %s" %(dcmtarpath,sorteddcm))
						print dcmsorter
						os.system(dcmsorter)
