import sys, os, glob, fnmatch, shutil,commands
src_dir = "/Volumes/Data_Meng_Canlab/Marie-wolfer-preprocess"
# dst_dir = "/Volumes/Data_Meng_Canlab/Weiqiang-rda"
# src_dir = "/Volumes/Data_Meng_Canlab/Marie-wolfer-rda"
# src_dir = "/Volumes/Data_Meng_Canlab/Weiqiang-rda"
subjids = os.listdir(src_dir)
subjids.sort()
subjids = ['bg52_1_Messung']

for subjid in subjids:
	if not subjid.startswith('.'):
		subjpath = os.path.join(src_dir,subjid)
		if os.path.isdir(subjpath):
			for rdapath, rdadirs, rdafiles in os.walk(subjpath):
				#print rdapath
				if 'MRS_' in rdapath and not 'T1_' in rdapath:
					os.chdir(rdapath)
					print subjid + ':' + rdapath
					for rdafile in rdafiles:
						if ('h20') in rdafile:
							#print rdapath
							newrda = rdafile.replace('h20','h2o')
							rename = ("mv %s %s" %(rdafile,newrda))
							print rename
							os.system(rename)
						elif ('water') in rdafile.lower():
							#print rdapath
							newrda = rdafile.replace('water','h2o')
							rename = ("mv %s %s" %(rdafile,newrda))
							print rename
							os.system(rename)
						elif ('_mrs') in rdafile.lower():
							#print rdapath
							newrda = rdafile.replace('_mrs','')
							rename = ("mv %s %s" %(rdafile,newrda))
							print rename
							os.system(rename)
						elif ('_h2o_') in rdafile.lower():
							#print rdapath
							newrda = rdafile.replace('_h2o_','_')
							newrda = newrda.replace('.rda','_h2o.rda')
							rename = ("mv %s %s" %(rdafile,newrda))
							print rename
							os.system(rename)	
						# elif ('_ws_h2o') in rdafile.lower():
						# 	#print rdapath
						# 	newrda = rdafile.replace('_ws_h2o','_h2o')
						# 	rename = ("mv %s %s" %(rdafile,newrda))
							#print rename	
						# elif ('_ws_') in rdafile.lower() and not ('_ws_h2o') in rdafile.lower():
						# 	#print rdapath
						# 	newrda = rdafile.replace('_ws_','_')
						# 	newrda = newrda.replace('.rda','_ws.rda')
						# 	rename = ("mv %s %s" %(rdafile,newrda))
						# 	#print rename
						# 	#os.system(rename)	
						elif ('-') in rdafile.lower():
							#print rdapath
							newrda = rdafile.replace('-','_')
							rename = ("mv %s %s" %(rdafile,newrda))
							print rename
							os.system(rename)		
						# elif ('.') in rdafile.lower():
						# 	#print rdapath
						# 	newrda = rdafile.replace('.','_')
						# 	rename = ("mv %s %s" %(rdafile,newrda))
						# 	#print rename
						# 	#os.system(rename)		


			for rdapath, rdadirs, rdafiles in os.walk(subjpath):
				if 'MRS_' in rdapath and not 'T1_' in rdapath:
					os.chdir(rdapath)
					print subjid + ':' + rdapath
					for rdafile in rdafiles:
						if '.DS_Store' in rdafile or '_15' in rdafile:
							rmrda = ("rm %s"%(rdafile))
							print rmrda
							os.system(rmrda)

						if not ('_h2') in rdafile.lower() and not '_ws.rda' in rdafile.lower() and '.rda' in rdafile:
							#print rdapath
							#print rdafile
							newrda = rdafile.replace('.rda','_ws.rda')
							rename = ("mv %s %s" %(rdafile,newrda))
							print rename
							os.system(rename)




 