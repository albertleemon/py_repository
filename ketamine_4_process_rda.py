import sys, os, glob, fnmatch, shutil,commands
from nipype.interfaces.freesurfer import ParseDICOMDir
from nipype.interfaces.fsl import BET
from nipype.interfaces.dcm2nii import Dcm2nii
from nipype.interfaces.fsl import ImageMaths
from nipype.interfaces.ants import N4BiasFieldCorrection
# from nipype.interfaces.freesurfer.preprocess import ReconAll
# from nipype.interfaces.freesurfer.utils import MakeAverageSubject
# from nipype.interfaces.io import DataGrabber
# import nipype.interfaces.io as nio
# import nipype.interfaces.freesurfer as fs    # freesurfer
# import nipype.interfaces.io as nio           # i/o routines
# import nipype.interfaces.matlab as mlab      # how to run matlab
import nipype.interfaces.spm as spm          # spm
# cpoy and rename 
# src = "/Volumes/Data_Meng_Canlab/Weiqiang-preprocess/"
src_dir = "/Volumes/Data_Meng_Canlab/Marie-wolfer-preprocess"
os.chdir(src_dir)
subjids = glob.glob("*_1_Messung")
#subjs = os.listdir(src)
#subjs.remove("ac49")
#"ky85","vl65_3"
#subjs = ["ky85", "md15", "mh58", "mm53_1", "mm53_2", "mm53_3", "nz56", "ob56", "ol12", "ou48", "ox84", "rb84_1", "rb84_2", "rb84_3", "rx84_1", "rx84_2", "rx84_3", "tg08", "tg29", "vl65_1", "vl65_2", "vl65_3", "wy22", "xk92", "yb58_1", "yb58_2", "yb58_3", "yd52_1", "yd52_2", "yd52_3", "yv98"]
#subjs = ["ac49"]
subjids.sort()
dst = src
# dst = "/Volumes/Data_Meng_Canlab/Weiqiang-preprocess/"
subjids = ["bh27_1_Messung"]
import nipype.interfaces.matlab as matlab
mlab = matlab.MatlabCommand()
for subjid in subjids:
	print subjid
	src_subjfolder = os.path.join(src_dir,subjid)
	folders = os.listdir(src_subjfolder)
	t1image_path_temp = os.path.join(src_subjfolder,"T1/imcalc/MPRAGE/convert_BET/convert_dicom/*.img")
	t1image_path = glob.glob(t1image_path_temp)
	t1folder = os.path.dirname(t1image_path[0])
	t1image = os.path.basename(t1image_path[0])
	print t1folder
	print t1image


	for folder in folders:
		rdaimage_path_temp = ("%s/*.rda"%(os.path.join(src_subjfolder,folder)))
		rdaimages = glob.glob(rdaimage_path_temp)
		if len(rdaimages)==2:
			for rdaimage in rdaimages:
				if not "_h2" in rdaimage.lower(): 
					rdafolder = os.path.dirname(rdaimages[0])
					rdaimage = os.path.basename(rdaimages[0])	
					os.chdir(rdafolder) 
					svs2mask = ("addpath(genpath('/Users/canlab06/Documents/Soft/matlab_addon/mrs_tools'));svs2mask_modified_meng(\'%s/\',\'%s\',\'%s/\',\'%s\')"%(t1folder,t1image,rdafolder,rdaimage))
					print svs2mask
					#svs2mask_modified_meng(t1folder,t1image.name,pgacc_rdafolder,pgacc_rdas)
					mlab.inputs.script = svs2mask
					out = mlab.run()
					rda_img_path = ("%s/*.img"%(os.chdir(os.path.join(src_subjfolder,folder)))
					rda_img = glob.glob(rda_folder_path)				

					nenMPRAGE = os.path.join(src,subj[0:3],"_1_Messung/T1/imcalc/nenMPRAGE.nii")

					n4 = N4BiasFieldCorrection()
					n4.inputs.dimension = 3
					n4.inputs.input_image = t1image[0]
					#n4.output_image = 'N4' + t1image
					# n4.inputs.bspline_fitting_distance = 300
					# n4.inputs.shrink_factor = 3
					# n4.inputs.n_iterations = [50,50,30,20]
					# n4.inputs.convergence_threshold = 1e-6
					n4.run()

					ants_rigid = ("ANTS 3 -m MI[%s,%s,1,32] -i 0 -o r_ --rigid-affine true" %(nenMPRAGE,t1image[0]))
					ants_wrap_t1 = ("WarpImageMultiTransform 3 %s %s r_Affine.txt -R %s "%(t1image[0],os.path.join(t1folder,'r_',t1image[0]),nenMPRAGE))
					ants_wrap_rda = ("WarpImageMultiTransform 3 %s %s r_Affine.txt -R %s "%(rda_img,os.path.join(rdafolder,'r_',rda_img),nenMPRAGE))
					print ants_rigid
					os.system(ants_rigid)