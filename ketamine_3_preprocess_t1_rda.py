import sys, os, glob, fnmatch, shutil,commands
src_dir = "/Volumes/HD/canlab/7T_ketamine/ketamin/"
dst_dir = "/Volumes/Data_Meng_Canlab/Marie-wolfer-preprocess"
subjids = os.listdir(src_dir)
subjids.remove('abnormal')
subjids.sort()
subjids = ['bg52']
for subjid in subjids:
	subj_src_dir = os.path.join(src_dir,subjid)		
	if not subjid.startswith('.') and os.path.isdir(subj_src_dir):
		messungs = ['1_Messung','2_Messung','3_Messung']
		for messung in messungs:
			print subjid + ':' + messung
			subj_src_dir_messung = os.path.join(subj_src_dir,messung)
			os.chdir(subj_src_dir_messung)

			subj_src_dir_messung = os.path.join(subj_src_dir,messung)
			subj_dst_dir = subjid + '_' + messung 
			subj_dst_dir_messung = os.path.join(dst_dir,subj_dst_dir)
			subj_dst_dir_messung_t1 = os.path.join(subj_dst_dir_messung,'T1')
			subj_dst_dir_messung_t1_mprage = os.path.join(subj_dst_dir_messung_t1,'MPRAGE')
			subj_dst_dir_messung_t1_ge = os.path.join(subj_dst_dir_messung_t1,'GE')
			subj_dst_dir_messung_mrs_amcc = os.path.join(subj_dst_dir_messung,'MRS_AMCC')
			subj_dst_dir_messung_mrs_pcc = os.path.join(subj_dst_dir_messung,'MRS_PCC')
			subj_dst_dir_messung_mrs_23c = os.path.join(subj_dst_dir_messung,'MRS_23C')
			subj_dst_dir_messung_mrs_pgacc = os.path.join(subj_dst_dir_messung,'MRS_PGACC')
			# subj_dst_dir_messung__messung_pmu = os.path.join(dst_dir,'PMU',subjid)
			# subj_dst_dir_messung__messung_rest = os.path.join(subj_dst_dir_messung_,'REST')
			# subj_dst_dir_messung__messung_task = os.path.join(subj_dst_dir_messung_,'TASK')


			if not os.path.exists(subj_dst_dir):
				os.mkdir(subj_dst_dir)
			if not os.path.exists(subj_dst_dir_messung):
				os.mkdir(subj_dst_dir_messung)	
				if not os.path.exists(subj_dst_dir_messung_t1):
					os.mkdir(subj_dst_dir_messung_t1)
				if not os.path.exists(subj_dst_dir_messung_t1_mprage):
					os.mkdir(subj_dst_dir_messung_t1_mprage)
				if not os.path.exists(subj_dst_dir_messung_t1_ge):
					os.mkdir(subj_dst_dir_messung_t1_ge)
				if not os.path.exists(subj_dst_dir_messung_mrs_amcc):
					os.mkdir(subj_dst_dir_messung_mrs_amcc)	
				if not os.path.exists(subj_dst_dir_messung_mrs_pcc):
					os.mkdir(subj_dst_dir_messung_mrs_pcc)			
				if not os.path.exists(subj_dst_dir_messung_mrs_23c):
					os.mkdir(subj_dst_dir_messung_mrs_23c)					
				if not os.path.exists(subj_dst_dir_messung_mrs_pgacc):
					os.mkdir(subj_dst_dir_messung_mrs_pgacc)	

				# rdafiles = os.listdir(subj_src_dir_messung)
				os.chdir(subj_src_dir_messung)
				rdafiles = glob.glob("*.rda")
				for rdafile in rdafiles:
					print rdafile
					if ("amcc" in rdafile.lower()) and not ("_15.rda" in rdafile) and not os.path.exists(os.path.join(subj_src_dir_messung,rdafile.replace('.rda','_1.rda'))):  
						cp_mrs = ("cp -r %s %s" %(os.path.join(subj_src_dir_messung,rdafile),subj_dst_dir_messung_mrs_amcc))
						print cp_mrs
						os.system(cp_mrs)
					elif ("23c" in rdafile.lower()) and not ("_15.rda" in rdafile) and not os.path.exists(os.path.join(subj_src_dir_messung,rdafile.replace('.rda','_1.rda'))): 
						cp_mrs = ("cp -r %s %s" %(os.path.join(subj_src_dir_messung,rdafile),subj_dst_dir_messung_mrs_23c))
						print cp_mrs
						os.system(cp_mrs)
					elif ("pcc" in rdafile.lower()) and not ("_15.rda" in rdafile) and not os.path.exists(os.path.join(subj_src_dir_messung,rdafile.replace('.rda','_1.rda'))):  
						cp_mrs = ("cp -r %s %s" %(os.path.join(subj_src_dir_messung,rdafile),subj_dst_dir_messung_mrs_pcc))
						print cp_mrs
						os.system(cp_mrs.lower())
					elif ("pgacc" in rdafile.lower()) and not ("_15.rda" in rdafile) and not os.path.exists(os.path.join(subj_src_dir_messung,rdafile.replace('.rda','_1.rda'))): 
						cp_mrs = ("cp -r %s %s" %(os.path.join(subj_src_dir_messung,rdafile),subj_dst_dir_messung_mrs_pgacc))
						print cp_mrs
						os.system(cp_mrs)

				for dcmpath, dcmdirs, dcmfiles in os.walk(subj_src_dir_messung):
					if ("iso" in os.path.dirname(dcmpath).lower()) and ("nd" in os.path.basename(dcmpath).lower()) and ("mprage" in os.path.basename(dcmpath).lower()):	
						print dcmpath
						if len(os.listdir(subj_dst_dir_messung_t1_mprage))<1:
							cp_mri = ("cp -r %s %s" %(dcmpath,subj_dst_dir_messung_t1_mprage))
							print cp_mri
							os.system(cp_mri)

					elif ("iso" in os.path.dirname(dcmpath).lower()) and ("nd" in os.path.basename(dcmpath).lower()) and ("_ge" in os.path.basename(dcmpath).lower()):	
						print dcmpath
						if len(os.listdir(subj_dst_dir_messung_t1_ge))<1:
							cp_mri = ("cp -r %s %s" %(dcmpath,subj_dst_dir_messung_t1_ge))
							print cp_mri
							os.system(cp_mri)
							
					# elif ("bold_run" in os.path.dirname(dcmpath).lower()) and ("moco" in os.path.basename(dcmpath).lower()) and (len(dcmfiles) == 155): 
					# 	print dcmpath
					# 	if len(os.listdir(subj_dst_dir_task)) <1:
					# 		cp_mri = ("cp -r %s %s" %(dcmpath,subj_dst_dir_task))
					# 		print cp_mri
					# 		os.system(cp_mri)

					# elif ("bold_trap" in os.path.dirname(dcmpath).lower()) and ("moco" in os.path.basename(dcmpath).lower()) and (len(dcmfiles) == 280): 
					# 	print dcmpath
					# 	if len(os.listdir(subj_dst_dir_rest)) <1:
					# 		cp_mri = ("cp -r %s %s" %(dcmpath,subj_dst_dir_rest))
					# 		print cp_mri
					# 		os.system(cp_mri)		