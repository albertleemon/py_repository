import sys, os, glob, fnmatch, shutil,commands
src_dir = "/Volumes/Data_Meng_Canlab/Marie-wolfer-preprocess"
# dst_dir = "/Volumes/Data_Meng_Canlab/Weiqiang-rda"
# src_dir = "/Volumes/Data_Meng_Canlab/Marie-wolfer-rda"
# src_dir = "/Volumes/Data_Meng_Canlab/Weiqiang-rda"
subjids = os.listdir(src_dir)
subjids.sort()
subjids = ['bg52_1_Messung']

dst_dir = "/Volumes/Data_Meng_Canlab/Marie-wolfer-rda-result"
# if not os.path.exists(dst_dir):
#  	os.mkdir(dst_dir)
for subjid in subjids:
	subj_src_dir = os.path.join(src_dir,subjid)
	if not subjid.startswith('.') and os.path.isdir(subj_src_dir): 
		subj_dst_dir = subjid +'_MRS_AMCC'
		subj_dst_dir_rda_amcc = os.path.join(dst_dir,(subjid +'_MRS_AMCC'))
		subj_dst_dir_rda_pgacc = os.path.join(dst_dir,(subjid +'_MRS_PGACC'))
		if not os.path.exists(subj_dst_dir_rda_amcc):
			os.mkdir(subj_dst_dir_rda_amcc)
			cp_mri_rda_amcc = ("cp -r %s/* %s" %(os.path.join(subj_src_dir,'MRS_AMCC'),subj_dst_dir_rda_amcc))
			print cp_mri_rda_amcc
			os.system(cp_mri_rda_amcc)

		if not os.path.exists(subj_dst_dir_rda_pgacc):
			os.mkdir(subj_dst_dir_rda_pgacc)
			cp_mri_rda_pgacc = ("cp -r %s/* %s" %(os.path.join(subj_src_dir,'MRS_PGACC'),subj_dst_dir_rda_pgacc))
			print cp_mri_rda_pgacc
			os.system(cp_mri_rda_pgacc)

