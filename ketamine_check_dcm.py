import sys, os, glob, fnmatch, shutil,commands
src_dir = "/Volumes/HD/canlab/7T_data_weiqiang/ketamin/"
dst_dir = "/Volumes/Data_Meng_Canlab/Marie-wolfer"
subjids = os.listdir(src_dir)
subjids = ["bh27"]
subjids.sort()

# myFile = open('/Volumes/Data_Meng_Canlab/Marie-wolfer/infos.txt', 'a')
# sys.stdout = myFile
for subjid in subjids:
	if not subjid.startswith('.') and os.path.isdir(os.path.join(src_dir,subjid)): 
		print subjid
		subj_src_dir = os.path.join(src_dir,subjid)
		subj_dst_dir = os.path.join(dst_dir,subjid)
		#messungs = ['1_Messung','2_Messung','3_Messung']
		messungs = ['1_Messung']
		for messung in messungs:
			print messung
			subj_src_dir_messung = os.path.join(subj_src_dir,messung)
			subj_dst_dir_messung = os.path.join(subj_dst_dir,messung)
			for dcmpath, dcmdirs, dcmfiles in os.walk(subj_src_dir_messung):
				#print dcmpath
				# matching = [s for s in dcmfiles if ".tar" in s]
				# print matching
				# if not matching == []:
				if ("bold_run" in os.path.dirname(dcmpath).lower()): 
					# print dcmpath
					print os.path.basename(dcmpath)					
					print len(dcmfiles)

				elif ("bold_trap" in os.path.dirname(dcmpath).lower()): 
					# print dcmpath					
					print os.path.basename(dcmpath)
					print len(dcmfiles)

				elif ("iso" in os.path.dirname(dcmpath).lower() and ("ge" in os.path.basename(dcmpath).lower()): 	
					# print dcmpath					
					print os.path.basename(dcmpath)
					print len(dcmfiles)

# myFile.close()					
