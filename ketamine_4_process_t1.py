import sys, os, glob, fnmatch, shutil,commands
from nipype.interfaces.freesurfer import ParseDICOMDir
from nipype.interfaces.fsl import BET
from nipype.interfaces.dcm2nii import Dcm2nii
from nipype.interfaces.fsl import ImageMaths
from nipype.interfaces.ants import N4BiasFieldCorrection
# from nipype.interfaces.freesurfer.preprocess import ReconAll
# from nipype.interfaces.freesurfer.utils import MakeAverageSubject
# from nipype.interfaces.io import DataGrabber
# import nipype.interfaces.io as nio
# import nipype.interfaces.freesurfer as fs    # freesurfer
# import nipype.interfaces.io as nio           # i/o routines
# import nipype.interfaces.matlab as mlab      # how to run matlab
import nipype.interfaces.spm as spm          # spm

# import nipype.interfaces.utility as util     # utility
import nipype.pipeline.engine as pe # pypeline engine
from nipype.pipeline.engine import Node, MapNode, Workflow

convert = pe.Node(Dcm2nii(), name = 'convert_dicom',)
skull_stripper = pe.Node(BET(mask=True,output_type = "NIFTI"), name = 'skull_stripper')
convert_flow = pe.Workflow(name = 'convert_BET')
convert_flow.connect([(convert, skull_stripper, [('converted_files','in_file')]),
					])

# math_mprage_ge = pe.Node(ImageMaths(),out_data_type = 'input', output_type = 'NIFTI_PAIR', suffix = 't')
# math_convert_flow = pe.Workflow(name = 'imcalc')	
# math_convert_flow.connect([(math_mprage_ge,[('converted_files','in_file')]),
# 					])
# n4_correction = pe.Node(N4BiasFieldCorrection(),name = 'n4_correction')
# convert_flow = pe.Workflow(name = 'convert_BET_N4')

# ds.iterfield = ('subject_id',subjs)
# convert_flow.connect(ds, 'dcm',convert,'source_names')
# convert_flow.connect([(convert, skull_stripper, [('converted_files','in_file')]),
# 					(skull_stripper, n4_correction,[('out_file','input_image')])
# 					])


# cpoy and rename 
# src = "/Volumes/Data_Meng_Canlab/Weiqiang-preprocess/"
src = "/Volumes/Data_Meng_Canlab/Marie-wolfer-preprocess"
os.chdir(src)
subjs = glob.glob("*_1_Messung")
#subjs = os.listdir(src)
#subjs.remove("ac49")
#"ky85","vl65_3"
#subjs = ["ky85", "md15", "mh58", "mm53_1", "mm53_2", "mm53_3", "nz56", "ob56", "ol12", "ou48", "ox84", "rb84_1", "rb84_2", "rb84_3", "rx84_1", "rx84_2", "rx84_3", "tg08", "tg29", "vl65_1", "vl65_2", "vl65_3", "wy22", "xk92", "yb58_1", "yb58_2", "yb58_3", "yd52_1", "yd52_2", "yd52_3", "yv98"]
#subjs = ["ac49"]
subjs.sort()
dst = src
# dst = "/Volumes/Data_Meng_Canlab/Weiqiang-preprocess/"

T1 = ['MPRAGE','GE']
for subj in subjs:
	print subj
	src_subjfolder = os.path.join(src,subj)
	for img in T1:
		subjfolder = os.path.join(src,subj,"T1",img)
		os.chdir(subjfolder)
		dcmfiles = glob.glob('*/*')
		dst_subjfolder = os.path.join(dst,subj,'T1','imcalc')
		if not os.path.exists(dst_subjfolder):
			os.mkdir(dst_subjfolder)
		dst_subjfolder_img = os.path.join(dst_subjfolder,img)
		if not subj.startswith('.') and not os.path.exists(dst_subjfolder_img):
			if not os.path.exists(dst_subjfolder_img):
				os.mkdir(dst_subjfolder_img)		
				convert_flow.inputs.convert_dicom.source_names = os.path.abspath(dcmfiles[0])
				convert_flow.base_dir = dst_subjfolder_img
				convert_flow.run()

		os.chdir(os.path.join(dst_subjfolder_img,'convert_BET/convert_dicom/'))
		t1 = glob.glob('20*.nii')
		print t1
		if img == 'MPRAGE':

			maths = ('fslmaths %s -thr 20 %s -odt int'%(t1[0],'t_'+t1[0]))
			t1_mprage = os.path.abspath(t1[0])
			print maths
			os.system(maths)
			tmp = glob.glob(os.path.join(dst_subjfolder_img,'convert_BET/convert_dicom/t_20*.nii.gz'))
			t1_mprage_t = os.path.abspath(tmp[0])
			# chtype = ('/usr/local/fsl/bin/fslchfiletype NIFTI_PAIR %s %s'%(t1[0],t1[0].replace('.nii','.img')))
			# print chtype
			# os.system(chtype)
			# print t1_mprage_t
		elif img == 'GE': 
			maths = ('fslmaths %s -thr 40 %s -odt int'%(t1[0],'t_'+t1[0]))
			t1_ge = os.path.abspath(t1[0])
			tmp = glob.glob(os.path.join(dst_subjfolder_img,'convert_BET/skull_stripper/20*_brain_mask.nii'))
			t1_ge_brain_mask = os.path.abspath(tmp[0])
			print maths				
			os.system(maths)
			tmp = glob.glob(os.path.join(dst_subjfolder_img,'convert_BET/convert_dicom/t_20*.nii.gz'))
			t1_ge_t = os.path.abspath(tmp[0])
			print t1_ge_t


	os.chdir(dst_subjfolder)

	enimg = 'enMPRAGE.nii.gz'
	maths = ('fslmaths %s -div %s -mul %s -uthr 1 -mul 1000 %s -odt int'%(t1_mprage_t,t1_ge_t,t1_ge_brain_mask,enimg))
	print maths
	os.system(maths)

	# nenimg = 'nenMPRAGE.nii.gz'
	# maths = ('fslmaths %s -uthr 1 -mul %s -mul 1000 %s -odt int'%(enimg,enimg,enimg))
	# print maths
	# os.system(maths)

	n4 = N4BiasFieldCorrection()
	n4.inputs.dimension = 3
	n4.inputs.input_image = enimg
#	n4.output_image = 'nenMPRAGE_corrected.nii'
	# n4.inputs.bspline_fitting_distance = 300
	# n4.inputs.shrink_factor = 3
	# n4.inputs.n_iterations = [50,50,30,20]
	# n4.inputs.convergence_threshold = 1e-6
	n4.run()
	# import nipype.interfaces.spm as spm
	# seg = spm.NewSegment()
	# seg.inputs.channel_files = 'nenMPRAGE_corrected.nii'
	# seg.inputs.channel_info = (0.0001, 60, (True, True))
	# seg.run() 

	nenimg = enimg.replace('.nii.gz','_corrected.nii.gz')
	chtype = ('/usr/local/fsl/bin/fslchfiletype NIFTI %s'%(nenimg))
	print chtype
	os.system(chtype)

	import nipype.interfaces.spm as spm

	seg = spm.Segment()
	seg.inputs.data = nenimg[0:-3]
	seg.gm_output_type = [False,False,True]
	seg.wm_output_type = [False,False,True]
	seg.csf_output_type = [False,False,False]
	seg.clean_masks = 'no'
	seg.gaussians_per_class = [2,2,2,4]
	seg.affine_regularization = 'mni'
	seg.warping_regularization = 1
	seg.warp_frequency_cutoff = 25
	seg.bias_regularization = 0.0001
	seg.bias_fwhm = 60
	seg.sampling_distance = 3
	seg.run() 

	os.chdir(src_subjfolder)
	rdas = glob.glob('MRS_*')
	for rda in rdas:
		print rda
